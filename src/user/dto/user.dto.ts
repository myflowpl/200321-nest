import { UserModel } from '../models';
import { ApiProperty } from '@nestjs/swagger';
import { IsString, MinLength, IsEmail } from 'class-validator';

export class UserRegisterRequestDto {
  @ApiProperty({
    example: 'Piotr'
  })
  name: string;
  @ApiProperty({
    example: 'piotr@myflow.pl'
  })
  email: string;
  @ApiProperty({
    example: '123',
    description: 'Minimum 3 znaki'
  })
  password: string;
}

export class UserRegisterResponseDto {
  @ApiProperty()
  user: UserModel;
}

export class UserLoginRequestDto {
  @ApiProperty({
    example: 'piotr@myflow.pl'
  })
  @IsEmail()
  email: string;
  @ApiProperty({
    example: '123'
  })
  @IsString()
  @MinLength(3)
  password: string;
}

export class UserLoginResponseDto {
  @ApiProperty()
  token: string;
  @ApiProperty()
  user: UserModel;
}
