import { Controller, Post, Body, UseInterceptors, Get, UseGuards, HttpException, HttpStatus, UsePipes, ValidationPipe, Query } from "@nestjs/common";
import { UserService } from "../services/user.service";
import { ApiCreatedResponse, ApiResponse, ApiBearerAuth } from "@nestjs/swagger";
import { UserRegisterResponseDto, UserRegisterRequestDto, UserLoginRequestDto, UserLoginResponseDto } from "../dto";
import { UserInterceptor } from "../interceptors/user.interceptor";
import { User } from "../decorators/user.decorator";
import { AuthGuard } from "../guards/auth.guard";
import { Roles } from "../decorators/roles.decorator";
import { UserRole } from "../models";
import { AuthService } from "../services/auth.service";

@Controller('user')
// @UseInterceptors(UserInterceptor)
export class UserController {

  constructor(
    private userService: UserService,
    private authService: AuthService,
  ) {}

  @Post('login')
  @ApiCreatedResponse({type: UserLoginResponseDto})
  @UsePipes(ValidationPipe)
  async login(@Body() credentials: UserLoginRequestDto): Promise<UserLoginResponseDto> {

    const user = await this.userService.findByCredentials(credentials.email, credentials.password);

    if (!user) {
      throw new HttpException('ValidationError', HttpStatus.UNPROCESSABLE_ENTITY);
    }
    return {
      token: await this.authService.tokenSign({user}),
      user,
    };
  }

  @Post('register')
  @ApiCreatedResponse({type: UserRegisterResponseDto})
  async register(@Body() data: UserRegisterRequestDto): Promise<UserRegisterResponseDto> {
    const user = await this.userService.create(data);
    // TODO handle errors
    return {
      user,
    };
  }

  @Get()
  @UseGuards(AuthGuard)
  @Roles(UserRole.ADMIN)
  @ApiBearerAuth()
  getUser(@User() user) {
    return {user};
  }
}
